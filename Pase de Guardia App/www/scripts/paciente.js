﻿/*PROBLEMAS

paciente.js:807 Uncaught TypeError: Cannot set property '0' of null
at cont_hem_toAR (paciente.js:807)
at create_div_HEMODINAMICO (paciente.js:662)
at hemodinamico_check (paciente.js:615)
at select_control (paciente.js:890)
at HTMLInputElement.onclick(paciente.html:21)

*/


var VIEW_PAC = 0;
var view_paciente = 0;
var viewpac1_butpressed = false;

var TAM = 0;

var control_n = 0;

//PROMPTS VALUES
var TASVALUE;
var TADVALUE;
var FCVALUE;

//arrays control hemodinamico
var control_hemodinamico_AR1 = [4];
var control_hemodinamico_AR2 = [4];
var control_hemodinamico_AR3 = [4];
var control_hemodinamico_AR4 = [4];
var control_hemodinamico_AR5 = [4];

var control_hemodinamico_n = 0;

var has_cont_hemodinamico_values = false;


//clock
var get_h = 0;
var get_m = 0;

//DEBUG
document.getElementById("debug_vercontrolAR").addEventListener("click", function () {
    console.log(control_hemodinamico_AR1);
});

document.getElementById("clear_localstorage").addEventListener("click", function () {
    localStorage.clear();
});

function load_conthem_hasvalue() {
    var load_hasvalue1 = localStorage.getItem("strd_cont_hem_hasvalue");
    var load_hasvalue1b = JSON.parse(load_hasvalue1);
    has_cont_hemodinamico_values = load_hasvalue1b;
    console.log(has_cont_hemodinamico_values);
}

//MONITORS-------------------------------------------------------------------------------

function mon_div_hemodin() {
    var hemodin_cont1 = document.getElementById("time control 1");

}


//monitors tas buts

//monitor tas but 1 press 
function mon_tasbut1() {
    var tasbut = document.getElementById("div_TAS1");

    if (tasbut) {
        document.getElementById("addtas_bu1").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");
           
            control_hemodinamico_AR1[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR1 1 which is: " + control_hemodinamico_AR1[1]);
            document.getElementById("TASval1").innerHTML = TASVALUE;

            calc_TAM();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);

            
            
            console.log("TAM to AR " + control_hemodinamico_AR1[3]);

            save_cont_hem1();

        }
    }
}

//monitor tas but 2 press
function mon_tasbut2() {
    var tasbut = document.getElementById("div_TAS2");

    if (tasbut) {
        document.getElementById("addtas_bu2").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");
            control_hemodinamico_AR2[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR2 1 which is: " + control_hemodinamico_AR2[1]);
            document.getElementById("TASval2").innerHTML = TASVALUE;

            calc_TAM();

            
            console.log("TAM to AR " + control_hemodinamico_AR2[3]);
        }
    }
}

//monitor tas but 3 press
function mon_tasbut3() {
    var tasbut = document.getElementById("div_TAS3");

    if (tasbut) {
        document.getElementById("addtas_bu3").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");
            control_hemodinamico_AR3[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR3 1 which is: " + control_hemodinamico_AR3[1]);
            document.getElementById("TASval3").innerHTML = TASVALUE;

            calc_TAM();

           
            console.log("TAM to AR " + control_hemodinamico_AR3[3]);
        }
    }
}


//monitor tas but 4 press
function mon_tasbut4() {
    var tasbut = document.getElementById("div_TAS4");

    if (tasbut) {
        document.getElementById("addtas_bu4").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");
            control_hemodinamico_AR4[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR4 1 which is: " + control_hemodinamico_AR4[1]);
            document.getElementById("TASval4").innerHTML = TASVALUE;

            calc_TAM();

            
            console.log("TAM to AR " + control_hemodinamico_AR4[3]);
        }
    }
}

//monitor tas but 5 press
function mon_tasbut5() {
    var tasbut = document.getElementById("div_TAS5");

    if (tasbut) {
        document.getElementById("addtas_bu5").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");
            control_hemodinamico_AR5[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR5 1 which is: " + control_hemodinamico_AR5[1]);
            document.getElementById("TASval5").innerHTML = TASVALUE;

            calc_TAM();

           
            console.log("TAM to AR " + control_hemodinamico_AR5[3]);
        }
    }
}

//monitor tad buttons
//monitor tad but 1 press
function mon_tadbut1() {
    var tadbut = document.getElementById("div_TAD1");

    if (tadbut) {
        document.getElementById("addtad_bu1").onclick = function () {
            TADVALUE = prompt("Ingresar Tension Arterial Diastolica");
            control_hemodinamico_AR1[2] = TADVALUE;
            console.log(TADVALUE);
            document.getElementById("TADval1").innerHTML = TADVALUE;

            calc_TAM();


          
            console.log("TAM to AR " + control_hemodinamico_AR1[3]);

            save_cont_hem1();
        }
    }
}

//monitor tad but 2 press
function mon_tadbut2() {
    var tadbut = document.getElementById("div_TAD2");

    if (tadbut) {
        document.getElementById("addtad_bu2").onclick = function () {
            TADVALUE = prompt("Ingresar Tension Arterial Diastolica");
            control_hemodinamico_AR2[2] = TADVALUE;
            console.log(TADVALUE);
            document.getElementById("TADval2").innerHTML = TADVALUE;

            calc_TAM();
        }
    }
}

//monitor tad but 3 press
function mon_tadbut3() {
    var tadbut = document.getElementById("div_TAD3");

    if (tadbut) {
        document.getElementById("addtad_bu3").onclick = function () {
            TADVALUE = prompt("Ingresar Tension Arterial Diastolica");
            control_hemodinamico_AR3[2] = TADVALUE;
            console.log(TADVALUE);
            document.getElementById("TADval3").innerHTML = TADVALUE;

            calc_TAM();
        }
    }
}

//monitor tad but 4 press
function mon_tadbut4() {
    var tadbut = document.getElementById("div_TAD4");

    if (tadbut) {
        document.getElementById("addtad_bu4").onclick = function () {
            TADVALUE = prompt("Ingresar Tension Arterial Diastolica");
            control_hemodinamico_AR4[2] = TADVALUE;
            console.log(TADVALUE);
            document.getElementById("TADval4").innerHTML = TADVALUE;

            calc_TAM();
        }
    }
}

//monitor tad but 5 press
function mon_tadbut5() {
    var tadbut = document.getElementById("div_TAD5");

    if (tadbut) {
        document.getElementById("addtad_bu5").onclick = function () {
            TADVALUE = prompt("Ingresar Tension Arterial Diastolica");
            control_hemodinamico_AR5[2] = TADVALUE;
            console.log(TADVALUE);
            document.getElementById("TADval5").innerHTML = TADVALUE;

            calc_TAM();
        }
    }
}

//monitor fc buttons
//monitor fc1 button
function mon_fcbut1() {
    var fcbut = document.getElementById("addfc_bu1");

    if (fcbut) {
        document.getElementById("addfc_bu1").onclick = function () {
            FCVALUE = prompt("Ingresar Frecuencia Cardiaca");
            control_hemodinamico_AR1[3] = FCVALUE;
            console.log(FCVALUE);
            document.getElementById("fcval1").innerHTML = FCVALUE;

            control_hemodinamico_AR1[3] = FCVALUE; 

            save_cont_hem1();
    
        }
    }
}


//save control hemodinamico AR to local storage
function save_cont_hem1() {
    var save_conthem1 = control_hemodinamico_AR1;
    var save_conthem1b = JSON.stringify(save_conthem1);
    localStorage.setItem("saved_cont_hem1", save_conthem1b);
    console.log(control_hemodinamico_AR1 + " SAVED");
} 

//load control hemodinamico AR from local storage
function load_cont_hem1() {
    if (has_cont_hemodinamico_values === true) {
        var load_conthem1 = localStorage.getItem("saved_cont_hem1");
        console.log(load_conthem1);
        var load_conthem1b = JSON.parse(load_conthem1);
        console.log(load_conthem1b);
        control_hemodinamico_AR1 = load_conthem1b;
        console.log(control_hemodinamico_AR1 + " LOADED");
    } else {
        console.log("control hemodinamico has no values");
    }

}


//create divs of loaded control hemidinamico AR
function create_div_onload() {

    if (control_hemodinamico_n >= 1) {
        control_n = 1;
        create_div_HEMODINAMICO();

        console.log("div 1 created");

        if (control_hemodinamico_n >= 2) {
            control_n = 2;
            create_div_HEMODINAMICO();
            console.log("div 2 created");

            if (control_hemodinamico_n >= 3) {
                control_n = 3;
                create_div_HEMODINAMICO();
                console.log("div 3 created");

                if (control_hemodinamico_n >= 4) {
                    control_n = 4;
                    create_div_HEMODINAMICO();
                    console.log("div 4 created");

                    if (control_hemodinamico_n === 5) {
                        control_n = 5;
                        create_div_HEMODINAMICO();
                        console.log("div 5 created");


                    }
                }

            }
        } 
    }   
        
    
}


//BACK BUTTON
document.getElementById("BUTindex_page").addEventListener("click", function () {
    window.location.href = "index.html"
});

//ONLOAD FX
function ONLOADPACVIEW() {

    //LOAD HEMODINAMICO CONTROL N IN LOCAL STORAGE
    load_conhem_n();
    //load control hemidinamico AR1
    load_cont_hem1();
    //control hemodinamico has values?
    load_conthem_hasvalue();

    console.log(control_n);
    console.log(control_hemodinamico_AR1);

    if (control_n === 1) {
        control_hemodinamico_AR1[0] = control_n;
        console.log("cont hem AR1 0 stored " + control_n);
    } else if (control_n === 2) {
        control_hemodinamico_AR2[0] = control_n;
        console.log("cont hem AR2 0 stored" + control_n);
    } else if (control_n === 3) {
        control_hemodinamico_AR3[0] = control_n;
        console.log("cont hem AR3 0 stored" + control_n);
    } else if (control_n === 4) {
        control_hemodinamico_AR4[0] = control_n;
        console.log("cont hem AR4 0 stored" + control_n);
    } else if (control_n === 5) {
        control_hemodinamico_AR5[0] = control_n;
        console.log("cont hem AR4 0 stored" + control_n);
    }


    //cargar pacientes
    var load_Logp0 = localStorage.getItem("store logp0");
    var load_Logp0_b = JSON.parse(load_Logp0);
    Log_p0 = load_Logp0_b;
    console.log(load_Logp0_b);

    var load_Logp1 = localStorage.getItem("store logp1");
    var load_Logp1_b = JSON.parse(load_Logp1);
    Log_p1 = load_Logp1_b;
    console.log(load_Logp1_b);

    var load_Logp2 = localStorage.getItem("store logp2");
    var load_Logp2_b = JSON.parse(load_Logp2);
    Log_p2 = load_Logp2_b;
    console.log(load_Logp2_b);

    var load_Logp3 = localStorage.getItem("store logp3");
    var load_Logp3_b = JSON.parse(load_Logp3);
    Log_p3 = load_Logp3_b;
    console.log(load_Logp3_b);

    var load_Logp4 = localStorage.getItem("store logp4");
    var load_Logp4_b = JSON.parse(load_Logp4);
    Log_p4 = load_Logp4_b;
    console.log(load_Logp4_b);

    var load_Logp5 = localStorage.getItem("store logp5");
    var load_Logp5_b = JSON.parse(load_Logp5);
    Log_p5 = load_Logp5_b;
    console.log(load_Logp5_b);

    var load_Logp6 = localStorage.getItem("store logp6");
    var load_Logp6_b = JSON.parse(load_Logp6);
    Log_p6 = load_Logp6_b;
    console.log(load_Logp6_b);

    var load_Logp7 = localStorage.getItem("store logp7");
    var load_Logp7_b = JSON.parse(load_Logp7);
    Log_p7 = load_Logp7_b;
    console.log(load_Logp7_b);

    var load_Logp8 = localStorage.getItem("store logp8");
    var load_Logp8_b = JSON.parse(load_Logp8);
    Log_p8 = load_Logp8_b;
    console.log(load_Logp8_b);

    var load_Logp9 = localStorage.getItem("store logp9");
    var load_Logp9_b = JSON.parse(load_Logp9);
    Log_p9 = load_Logp9_b;
    console.log(load_Logp9_b);

    adddata_topage();
 

    //reloj
    startTime();

    // setea cantidad de divs creados junto a create_div_onload
    if (control_hemodinamico_n === 1) {
        create_div_onload();
                                //ver aca
    } else if (control_hemodinamico_n === 2) {
        create_div_onload();
      

    } else if (control_hemodinamico_n === 3) {
        create_div_onload();
      
    } else if (control_hemodinamico_n === 4) {
        create_div_onload();
       
    } else if (control_hemodinamico_n === 5) {
        create_div_onload();
       
    }

    //add_datato_cont1();

  

   /*mon_adddatato_cont1();
   mon_tasbut1();
   mon_tadbut1();
   mon_fcbut1();*/
   mon_tasval1();

    

}



//FUNCIONES ONLOAD

function adddata_topage() {//carga la ficha del paciente

    //load which ver pac buton is clicked

    //pac1
    var load_verpac1but = localStorage.getItem("strd_pacbu1");
    var load_verpac1butb = JSON.parse(load_verpac1but);
    viewpac1_butpressed = load_verpac1butb;
    console.log(viewpac1_butpressed + " paciente 1");
    //pac2
    var load_verpac2but = localStorage.getItem("strd_pacbu2");
    var load_verpac2butb = JSON.parse(load_verpac2but);
    viewpac2_butpressed = load_verpac2butb;
    console.log(viewpac2_butpressed + " paciente 2");
    //pac3
    var load_verpac3but = localStorage.getItem("strd_pacbu3");
    var load_verpac3butb = JSON.parse(load_verpac3but);
    viewpac3_butpressed = load_verpac3butb;
    console.log(viewpac3_butpressed + " paciente 3");
    //pac4
    var load_verpac4but = localStorage.getItem("strd_pacbu4");
    var load_verpac4butb = JSON.parse(load_verpac4but);
    viewpac4_butpressed = load_verpac4butb;
    console.log(viewpac4_butpressed + " paciente 4");
    //pac5
    var load_verpac5but = localStorage.getItem("strd_pacbu5");
    var load_verpac5butb = JSON.parse(load_verpac5but);
    viewpac5_butpressed = load_verpac5butb;
    console.log(viewpac5_butpressed + " paciente 5");
    //pac6
    var load_verpac6but = localStorage.getItem("strd_pacbu6");
    var load_verpac6butb = JSON.parse(load_verpac6but);
    viewpac6_butpressed = load_verpac6butb;
    console.log(viewpac6_butpressed + " paciente 6");
    //pac7
    var load_verpac7but = localStorage.getItem("strd_pacbu7");
    var load_verpac7butb = JSON.parse(load_verpac7but);
    viewpac7_butpressed = load_verpac7butb;
    console.log(viewpac7_butpressed + " paciente 7");
    //pac8
    var load_verpac8but = localStorage.getItem("strd_pacbu8");
    var load_verpac8butb = JSON.parse(load_verpac8but);
    viewpac8_butpressed = load_verpac8butb;
    console.log(viewpac8_butpressed + " paciente 8");
    //pac9
    var load_verpac9but = localStorage.getItem("strd_pacbu9");
    var load_verpac9butb = JSON.parse(load_verpac9but);
    viewpac9_butpressed = load_verpac9butb;
    console.log(viewpac9_butpressed + " paciente 9");
    //pac10
    var load_verpac10but = localStorage.getItem("strd_pacbu10");
    var load_verpac10butb = JSON.parse(load_verpac10but);
    viewpac10_butpressed = load_verpac10butb;
    console.log(viewpac10_butpressed + " paciente 10");



    if (viewpac1_butpressed === true){
    var adddata_logp0_cama = Log_p0[0];
    document.getElementById("cama").innerHTML = adddata_logp0_cama;

    var adddata_logp0_apellido = Log_p0[1];
    document.getElementById("apellido").innerHTML = adddata_logp0_apellido;

    var adddata_logp0_sexo = Log_p0[2];
    document.getElementById("sexo").innerHTML = adddata_logp0_sexo;
    } else if (viewpac2_butpressed === true) {
        var adddata_logp1_cama = Log_p1[0];
        document.getElementById("cama").innerHTML = adddata_logp1_cama;

        var adddata_logp1_apellido = Log_p1[1];
        document.getElementById("apellido").innerHTML = adddata_logp1_apellido;

        var adddata_logp1_sexo = Log_p1[2];
        document.getElementById("sexo").innerHTML = adddata_logp1_sexo;
    } else if (viewpac3_butpressed === true) {
        var adddata_logp2_cama = Log_p2[0];
        document.getElementById("cama").innerHTML = adddata_logp2_cama;

        var adddata_logp2_apellido = Log_p2[1];
        document.getElementById("apellido").innerHTML = adddata_logp2_apellido;

        var adddata_logp2_sexo = Log_p2[2];
        document.getElementById("sexo").innerHTML = adddata_logp2_sexo;
    } else if (viewpac4_butpressed === true) {
        var adddata_logp3_cama = Log_p3[0];
        document.getElementById("cama").innerHTML = adddata_logp3_cama;

        var adddata_logp3_apellido = Log_p3[1];
        document.getElementById("apellido").innerHTML = adddata_logp3_apellido;

        var adddata_logp3_sexo = Log_p3[2];
        document.getElementById("sexo").innerHTML = adddata_logp3_sexo;
    } else if (viewpac5_butpressed === true) {
        var adddata_logp4_cama = Log_p4[0];
        document.getElementById("cama").innerHTML = adddata_logp4_cama;

        var adddata_logp4_apellido = Log_p4[1];
        document.getElementById("apellido").innerHTML = adddata_logp4_apellido;

        var adddata_logp4_sexo = Log_p4[2];
        document.getElementById("sexo").innerHTML = adddata_logp4_sexo;
    } else if (viewpac6_butpressed === true) {
        var adddata_logp5_cama = Log_p5[0];
        document.getElementById("cama").innerHTML = adddata_logp5_cama;

        var adddata_logp5_apellido = Log_p5[1];
        document.getElementById("apellido").innerHTML = adddata_logp5_apellido;

        var adddata_logp5_sexo = Log_p5[2];
        document.getElementById("sexo").innerHTML = adddata_logp5_sexo;
    } else if (viewpac7_butpressed === true) {
        var adddata_logp6_cama = Log_p6[0];
        document.getElementById("cama").innerHTML = adddata_logp6_cama;

        var adddata_logp6_apellido = Log_p6[1];
        document.getElementById("apellido").innerHTML = adddata_logp6_apellido;

        var adddata_logp6_sexo = Log_p6[2];
        document.getElementById("sexo").innerHTML = adddata_logp6_sexo;
    } else if (viewpac8_butpressed === true) {
        var adddata_logp7_cama = Log_p7[0];
        document.getElementById("cama").innerHTML = adddata_logp7_cama;

        var adddata_logp7_apellido = Log_p7[1];
        document.getElementById("apellido").innerHTML = adddata_logp7_apellido;

        var adddata_logp7_sexo = Log_p7[2];
        document.getElementById("sexo").innerHTML = adddata_logp7_sexo;
    } else if (viewpac9_butpressed === true) {
        var adddata_logp8_cama = Log_p8[0];
        document.getElementById("cama").innerHTML = adddata_logp8_cama;

        var adddata_logp8_apellido = Log_p8[1];
        document.getElementById("apellido").innerHTML = adddata_logp8_apellido;

        var adddata_logp8_sexo = Log_p8[2];
        document.getElementById("sexo").innerHTML = adddata_logp8_sexo;
    } else if (viewpac10_butpressed === true) {
        var adddata_logp9_cama = Log_p9[0];
        document.getElementById("cama").innerHTML = adddata_logp9_cama;

        var adddata_logp9_apellido = Log_p9[1];
        document.getElementById("apellido").innerHTML = adddata_logp9_apellido;

        var adddata_logp9_sexo = Log_p9[2];
        document.getElementById("sexo").innerHTML = adddata_logp9_sexo;
    } 
}


//check hemodinamico
function hemodinamico_check() {

    var sel_cont = document.getElementsByName("sel_controles");
    if (sel_cont[0].checked) {
        //get_time();

        //sum control hemodinamico
        var sumcont = control_hemodinamico_n + 1;
        control_hemodinamico_n = sumcont;
        console.log(control_hemodinamico_n);

        var save_conhem_n = control_hemodinamico_n;
        var save_conhem_nb = JSON.stringify(save_conhem_n);
        console.log(save_conhem_nb);
        localStorage.setItem("stored_conhem_n", save_conhem_nb);

        if (control_n <= 5) {
            create_div_HEMODINAMICO();
        }

    }

}




//CONTROLES FX

//HEMODINAMICO

function create_div_HEMODINAMICO() {


    var div_controles = document.getElementById("controles");// get div to append

    var cr_bu1 = document.createElement("button");
    var cr_bu2 = document.createElement("button");
    var cr_bu3 = document.createElement("button");//FC

    var cr_div = document.createElement("div"); //create div
    var cr_div2 = document.createElement("div");
    var cr_div3 = document.createElement("div");
    var cr_div4 = document.createElement("div");//TAD titulo
    var cr_p = document.createElement("p"); // create p

    var cr_p_tas_tit = document.createElement("p");//TAS titulo
    var cr_p_tas_val = document.createElement("p");//TAS value 

    var cr_p_tad_tit = document.createElement("p");//TAD titulo
    var cr_p_tad_val = document.createElement("p");//TAD value

    var cr_p_tam_tit = document.createElement("p");//TAM titulo
    var cr_p_tam_value = document.createElement("p");//TAM value

    var cr_p_fc_tit = document.createElement("p");//FC titulo
    var cr_p_fc_value = document.createElement("p");//FC value

    //CONTROL N
    div_controles.appendChild(cr_div);//append div to controles div
    cr_div.setAttribute("class", "hemod_control_individ")

    cr_div.setAttribute("id", "control nº" + control_n);// crear control number

   

    //TITULO
    cr_div.appendChild(cr_div2);//div titulo
    cr_div2.setAttribute("id", "titulo" + control_n);
    cr_div2.setAttribute("class", "hemod_control_individ");
    document.getElementById("titulo" + control_n).innerHTML = "Control Hemodinamico Nº " + control_n;


    //agregar TAS button
    document.getElementById("titulo" + control_n).appendChild(cr_div3);//TAS
    cr_div3.setAttribute("id", "div_TAS" + control_n);
    cr_div3.appendChild(cr_bu1);
    cr_bu1.setAttribute("id", "addtas_bu" + control_n);
    document.getElementById("addtas_bu" + control_n).innerHTML = "Agregar TAS";

    //TAS titulo
    cr_div3.appendChild(cr_p_tas_tit);
    cr_p_tas_tit.setAttribute("id", "P TAS" + control_n);
    cr_p_tas_tit.setAttribute("class", "TAS title");
    document.getElementById("P TAS" + control_n).innerHTML = "Tension Arterial Sistolica";

    //TAS value
    cr_div3.appendChild(cr_p_tas_val);
    cr_p_tas_val.setAttribute("id", "TASval" + control_n);
    cr_p_tas_val.setAttribute("class", "values");
    document.getElementById("TASval" + control_n).innerHTML = "TAS?";

    //agregar TAD button
    document.getElementById("titulo" + control_n).appendChild(cr_div4);//TAD
    cr_div4.setAttribute("id", "div_TAD" + control_n);
    cr_div4.appendChild(cr_bu2);
    cr_bu2.setAttribute("id", "addtad_bu" + control_n);
    document.getElementById("addtad_bu" + control_n).innerHTML = "Agregar TAD";

    //TAD titulo
    cr_div4.appendChild(cr_p_tad_tit);
    cr_p_tad_tit.setAttribute("id", "P TAD" + control_n);
    cr_p_tad_tit.setAttribute("class", "TAD title");
    document.getElementById("P TAD" + control_n).innerHTML = "Tension Arterial Diastolica";

    //TAD value
    cr_div4.appendChild(cr_p_tad_val);
    cr_p_tad_val.setAttribute("id", "TADval" + control_n);
    cr_p_tad_val.setAttribute("class", "values");
    document.getElementById("TADval" + control_n).innerHTML = "TAD?";

    //TAM titulo
    cr_div4.appendChild(cr_p_tam_tit);
    cr_p_tam_tit.setAttribute("id", "tamtit" + control_n);
    cr_p_tam_tit.setAttribute("class", "TAM title");
    document.getElementById("tamtit" + control_n).innerHTML = "Tension Arterial Media";

    //TAM value
    cr_div4.appendChild(cr_p_tam_value);
    cr_p_tam_value.setAttribute("id", "tamval" + control_n);
    cr_p_tam_value.setAttribute("class", "value");
    document.getElementById("tamval" + control_n).innerHTML = TAM;

    //agregar FC button
    cr_div4.appendChild(cr_bu3);
    cr_bu3.setAttribute("id", "addfc_bu" + control_n);
    document.getElementById("addfc_bu" + control_n).innerHTML = "Agregar FC";

    //FC titulo
    cr_div4.appendChild(cr_p_fc_tit);
    cr_p_fc_tit.setAttribute("id", "fctit" + control_n);
    cr_p_fc_tit.setAttribute("class", "FC title");
    document.getElementById("fctit" + control_n).innerHTML = "Frecuencia Cardiaca";

    //FC value
    cr_div4.appendChild(cr_p_fc_value);
    cr_p_fc_value.setAttribute("id", "fcval" + control_n);
    cr_p_fc_value.setAttribute("class", "value");
    document.getElementById("fcval" + control_n).innerHTML = "FC?";

    //hora
    cr_div.appendChild(cr_p);
    cr_p.setAttribute("id", "time control " + control_n);
    document.getElementById("time control " + control_n).innerHTML = "Hora: " + get_h + ":" + get_m;

    //cont_hem_toAR();// todo ok 

}


//RELOJ
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
        h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);

    get_h = h;
    get_m = m;

}
function checkTime(i) {
    if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
    return i;
}

//add TAS fx

function add_TAS() {

    var input_tas = prompt("Ingresar Tension Arterial Sistolica");

    document.getElementById("TAS" + control_n).innerHTML = input_tas;

  
}

//CALCULAR TAM
function calc_TAM() {
    var c_tas = TASVALUE;
    var c_tad = TADVALUE;

    var c1 = c_tad * 2;
    console.log(c1);
    var c2 = +c1 + +c_tas;
    console.log(c2);
    var c3 = c2 / 3;
    console.log(c3);
    TAM = c3;
    console.log(TAM);

    control_hemodinamico_AR1[4] = TAM;

    document.getElementById("tamval1").innerHTML = TAM;

    


}


//set control hemodinamico to proper array
function cont_hem_toAR() {
    if (control_n === 1) {
        control_hemodinamico_AR1[0] = control_n;
        console.log("cont hem AR1 0 stored " + control_n);
    } else if (control_n === 2) {
        control_hemodinamico_AR2[0] = control_n;
        console.log("cont hem AR2 0 stored" + control_n);
    } else if (control_n === 3) {
        control_hemodinamico_AR3[0] = control_n;
        console.log("cont hem AR3 0 stored" + control_n);
    } else if (control_n === 4) {
        control_hemodinamico_AR4[0] = control_n;
        console.log("cont hem AR4 0 stored" + control_n);
    } else if (control_n === 5) {
        control_hemodinamico_AR5[0] = control_n;
        console.log("cont hem AR4 0 stored" + control_n);
    }
}



// monitor tas values
//1
function mon_tasval1() {
    var mon_tas1 = document.getElementById("TASval1");

    if (TASval1) {
        document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1[1];
    }
}


//add tas tad fc data to div 1
function add_datato_cont1() {
    var tas1 = document.getElementById("TASval1");
    var tad1 = document.getElementById("TADval1");
    var fc1 = document.getElementById("fcval1");
    var tam1 = document.getElementById("tamval1");

    if (tas1) {
        document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1[1];
    } else if (tad1) {
        document.getElementById("TADval1").innerHTML = control_hemodinamico_AR1[2];
    } else if (fc1) {
        document.getElementById("fcval1").innerHTML = control_hemodinamico_AR1[3];
    } else if (tam1) {
        document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1[4];
    }
}

/*add tas tad fc data to div 1 backup
function add_datato_cont1() {
    document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1[1];
    document.getElementById("TADval1").innerHTML = control_hemodinamico_AR1[2];
    document.getElementById("fcval1").innerHTML = control_hemodinamico_AR1[3];
    document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1[4];
}*/


function load_conhem_n() {
    var load_control_hemod_n = localStorage.getItem("stored_conhem_n");
    var load_control_hemod_nb = JSON.parse(load_control_hemod_n);
    control_hemodinamico_n = load_control_hemod_nb;
    console.log("control hemodinam n is: " + control_hemodinamico_n);
} 

//SELECT CONTROLES FX

function select_control() {  //runs when hemodinamico is clicked

    console.log("control_n is " + control_n);

    var addcontrol = control_n + 1;
    control_n = addcontrol;
    console.log("control number " + control_n);

  


    //alert maximo controles
    if (control_n === 5) {
        alert("maxima cantidad de controles");
    }


    hemodinamico_check();

    mon_div_hemodin();
    mon_tasbut1();
    mon_tadbut1();
    mon_fcbut1();

    mon_tasbut2();
    mon_tadbut2();

    mon_tasbut3();
    mon_tadbut3();

    mon_tasbut4();
    mon_tadbut4();

    mon_tasbut5();
    mon_tadbut5();



    //continuar, crear mas monitors para los distintos id de los botones de tas y tad
}
